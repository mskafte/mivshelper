#include <cstdlib>
#include <string>
#include <iostream>

using namespace std;

void listExample1() {
  int list[6] = { 0, 1, 2, 3, 4, 5 };
  int length = sizeof(list)/sizeof(int);
  
  cout << "Length of list: " << length << endl;
  cout << "Content of list: ";
  
  for ( int elementPtr = 0; elementPtr < length; elementPtr++ ) {
    cout << list[elementPtr] << " ";
  }
  
  cout << endl;
}

int getRandomInt(const int maxValue) {
  int randomInt = rand();
  randomInt %= maxValue;
  cout << randomInt << endl;
  return randomInt;
}

void testRandomInt(const int maxValue, const int iterations) {
  for ( int i = 0; i < iterations; i++ )
    getRandomInt(maxValue);
}

int main(int argc, char** argv) {
  cout << "---" << endl;
  listExample1();
  cout << "---" << endl;
  testRandomInt(10, 8);
  cout << "---" << endl;
  
  return 0;
}
